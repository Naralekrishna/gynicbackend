module.exports = function (app) {
  const authController = require("../controllers/auth/authController");

  app.post("/api/v1/signup", authController.signup);
  app.post("/api/v1/signin", authController.signin);
  app.put("/api/v1/logout", authController.logout);
};
