const jwt = require("jsonwebtoken");
const config = require("../config/config.js");
const db = require("../config/db.config.js");

const Jwttokon = db.jwttokon;
verifyToken = (req, res, next) => {
  let token = req.headers["x-access-token"];

  if (!token) {
    return res.status(403).send({ auth: false, message: "No token provided." });
  }

  jwt.verify(token, config.secret, (err, decoded) => {
    if (err) {
      return res.status(500).send({
        auth: false,
        message: "Fail to Authentication. Error -> " + err,
      });
    }
    req.userId = decoded.id;
    next();
  });
};

verifyTokenDB = (req, res, next) => {
  let token = req.header("x-access-token");

  if (!token) {
    return res.status(403).send({ auth: false, message: "No token provided." });
  }
  Jwttokon.findOne({
    where: { Token: token, Status: "1" },
  })
    .then((jwttokon) => {
      if (!jwttokon) {
        return res.status(401).json({ failed: "Unauthorized Access " });
      }
      next();
    })
    .catch((err) => {
      res.status(500).send("Error -> " + err);
    });
};
const authJwt = {};
authJwt.verifyTokenDB = verifyTokenDB;
authJwt.verifyToken = verifyToken;
module.exports = authJwt;
