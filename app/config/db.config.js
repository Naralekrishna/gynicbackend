const Sequelize = require("sequelize");
const env = require("./env.js");
const sequelize = new Sequelize(env.database, env.username, env.password, {
  host: env.host,
  port: env.port,
  dialect: env.dialect,
  operatorsAliases: 0,
  logging: false,

  pool: {
    max: env.max,
    min: env.pool.min,
    acquire: env.pool.acquire,
    idle: env.pool.idle,
  },
});
const db = {};
db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.user = require("../models/User")(sequelize, Sequelize);
db.jwttokon = require("../models/JwtToken")(sequelize, Sequelize);

// Here we can connect users and jwttoken base on users'id
// db.user.hasMany(db.jwttokon, { foreignKey: "id" });
// db.jwttokon.belongsTo(db.users, { foreignKey: "UserId" });

module.exports = db;
