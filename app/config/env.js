const Sequelize = require("sequelize");

const env = {
  database: "gynic",
  username: "root",
  password: "",
  host: "0.0.0.0",
  dialect: "mysql",

  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000,
  },
};

module.exports = env;
