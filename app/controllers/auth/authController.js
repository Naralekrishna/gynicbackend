const db = require("../../config/db.config.js");
const config = require("../../config/config.js");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const User = db.user;
const JWTToken = db.jwttokon;

const Sequelize = require("sequelize");
const Op = Sequelize.Op;

exports.signup = (req, res) => {
  if (!req.body.email) {
    return res
      .status(400)
      .send({ message: "Your request Email is missing details." });
  } else {
    if (!req.body.password) {
      return res
        .status(400)
        .send({ message: "Your request Password is missing details." });
    } else {
      // if (!req.body.UserName) {
      // 	return res.status(400).send({ message:"Your request UserName is missing details."});
      // }
      // else {
      const EmailToValidate = req.body.email;
      const passToValidate = req.body.password;
      const EmailRegexp = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
      const mailcheck = EmailRegexp.test(EmailToValidate);
      const Passwordcheck = /^.{2,12}$/;
      const passcheck = Passwordcheck.test(passToValidate);
      if (!mailcheck) {
        res.status(500).json({
          message: "Please Enter A Correct Email like abc@example.com ",
        });
      } else {
        if (!passcheck) {
          res.status(500).json({
            message: "Password length should be Minimum 2 and Maximum 12",
          });
        } else {
          bcrypt.hash(req.body.password, 10, function (err, hash) {
            if (err) {
              return res.status(500).json({
                error: err,
              });
            } else {
              User.findOne({ where: { email: req.body.email } }).then(function (
                user
              ) {
                if (!user) {
                  const user = new User({
                    email: req.body.email,
                    //UserName: req.body.UserName,
                    password: hash,
                  });

                  user
                    .save()
                    .then(function (result) {
                      const JWTToken1 = jwt.sign(
                        { email: user.email, id: user.id },
                        config.secret,
                        {
                          //	expiresIn: 86400 // expires in 24 hours
                        }
                      );
                      const uid = user.id;
                      const jwttokon = new JWTToken({
                        userId: uid,
                        status: true,
                        token: JWTToken1,
                      });
                      jwttokon.save().then(function (result) {
                        res.status(200).json({
                          id: uid,
                          success: "200",
                          message: "New user has been created",
                          token: JWTToken1,
                        });
                      });
                    })
                    .catch((error) => {
                      res.status(500).json({
                        error: err,
                      });
                    });
                } else {
                  console.log("user is already present");
                  res.status(500).json({
                    success: "404",
                    message: "User Already Present in database",
                  });
                }
              });
            }
          });
        }
      }
      // }
    }
  }
};

exports.signin = (req, res) => {
  if (!req.body.email) {
    return res
      .status(401)
      .json({ message: " Your request email is missing details. " });
  } else {
    if (!req.body.password) {
      return res
        .status(401)
        .json({ message: "Your request username is missing details." });
    }
  }

  User.findOne({ where: { email: req.body.email } }).then((user) => {
    if (!user) {
      return res.status(401).json({
        success: "404",
        message: "Unauthorized Access,User Account Is Deactivated",
      });
    } else {
      User.findOne({
        where: { email: req.body.email /*, Password: req.body.Password,*/ },
      })
        .then((user) => {
          if (!user) {
            return res.status(401).json({
              success: "404",
              message: "Unauthorized Access,Enter Correct Email And Password",
            });
          }
          var passwordIsValid = bcrypt.compareSync(
            req.body.password,
            user.password
          );
          if (!passwordIsValid) {
            return res.status(401).json({
              success: "404",
              message: "Unauthorized Access,Enter Correct Email And Password",
            });
          }
          // const userid = user.userid;
          const jwt = require("jsonwebtoken");
          const JWTToken1 = jwt.sign(
            { email: user.email, id: user.id },
            config.secret,
            {}
          );
          const uid = user.id;
          const jwttokon = new JWTToken({
            userId: uid,
            status: true,
            token: JWTToken1,
          });

          jwttokon.save().then(function (result) {
            res.status(200).json({
              success: "200",
              message: "Welcome To The Application",
              id: uid,
              token: JWTToken1,
            });
          });
        })
        .catch((err) => {
          res.status(500).send("Error -> " + err);
        });
    }
  });
};

exports.logout = (req, res) => {
  const userId = req.body.id;
  let token = req.header("x-access-token");

  JWTToken.update(
    { status: "0" },
    { where: { userId: userId, token: token } }
  ).then((jwttokon) => {
    res.status(200).json({
      success: "200",
      message: "logout Successfully..",
      userId: userId,
    });
  });
};
